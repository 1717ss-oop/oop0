/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop0;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import oop.Wuerfel;

/**
 * modelliert ein Spiel, verwaltet den Ablauf des Spiels
 * @author Leonard Krause
 */

public class Spiel {
    private int variante;
    private Spieler[] spieler;
    private int rundenAnzahl;
    private int ziffernAnzahl;
    private String regeln;
    private static Scanner scan = new Scanner (System.in);
    /**
     * erstellt Objekt Spiel, setzt default values
     */
    public Spiel(){
        rundenAnzahl = 0;
        variante = -1;
        ziffernAnzahl = 3;
        regeln = "REGELN: HAUSNUMMERN \n"
        		+"Jeder Spieler wuerfelt dreimal hintereinander und entscheidet, vor/nach jedem Wurf, \nan welcher Stelle seiner Hausnummer die jeweilige Ziffer stehen soll. Ziel ist es, dabei die h�chst m�gliche Hausnummer pro Runde zu erreichen. \n"
        		+"Varianten:\n"
        		+"1. 	Die Position der jeweiligen Ziffer wird NACH jedem Wurf festgelegt\n"
        		+"2.	Die Position der jeweiligen Ziffer wird VOR jedem Wurf festgelegt\n"
        		+"Rundensieger: die h�chste Hausnummer der Runde\n"	
        		+"Gesamtsieger: Die Hausnummern aller Runden werden addiert. Der Spieler mit der h�chsten Gesamtzahl gewinnt.";
        
    }
    /**
     * erstellt ein neues Spiel, verwaltet Nutzereingaben (Einstellungen) für dieses Spiel
     */
    public void createGame(){
        String in = "-";
        while (!in.equals("y") && !in.equals("n")){
            System.out.println("Regeln anzeigen? (y oder n)");
            in=scan.next();
        }
        if (in.equals("y")){
            printRegeln();
        }
        variante=-1;
        while (variante < 0){
            try{
                System.out.println("Welche Variante wollt ihr spielen? (1|2):");
                variante=scan.nextInt();
                if (variante < 1 || variante > 2) variante=-1;
            }catch (Exception e){
                scan = new Scanner (System.in);
                System.out.println("Prüfe deine Eingabe! Optionen: (1|2)");
            }
        }
        rundenAnzahl =-1;
        while (rundenAnzahl < 1){
            try{
                System.out.println("Bitte die Anzahl der Runden angeben (1..n):");
                rundenAnzahl=scan.nextInt();
            }
            catch (Exception e){
                scan = new Scanner (System.in);
                System.out.println("Prüfe deine Eingabe! Optionen: [1-n]");
            }
        } 
        System.out.println("Bitte gib die Namen der Mitspieler ein. \n(Name{Enter} Name{Enter} usw. \":q\"{Enter} beendet die Eingabe):");
        List<String> spieler = new ArrayList<String>();
        in="";
        while (true){
            in = scan.nextLine();
            if (in.equalsIgnoreCase(":q")){
                break;
            }else if(spieler.contains(in)){
                System.out.println("Die Namen muessen eindeutig sein.");
            }else if(!in.isEmpty()){
                spieler.add(in);
            }
        }
        this.spieler = new Spieler[spieler.size()];
        for (int i=0; i<=spieler.size()-1;i++){
            this.spieler[i] = new Spieler(spieler.get(i),rundenAnzahl);
        }
    }
    /**
     * spielen des mit createGame erzeugten Spiels (ein Spiel kann mehrmals gespielt werden), Spielablauf
     */
    public void spielen(){
        if (variante == -1) createGame();
        else{
            String in = "-";
            while (!in.equals("y") && !in.equals("n")){
                System.out.println("Wieder mit denselben Eingaben spielen? (y|n):");
                in=scan.next();
            }
            if (in.equals("n")) createGame();
        }
        
        Wuerfel wuerfel = new Wuerfel(0,9);
        for (int i=0; i<=rundenAnzahl-1;i++){
            for (int k=0; k<=spieler.length-1;k++){
                System.out.println("Runde " + String.valueOf(i+1));
                spieler[k].yourTurn(wuerfel, variante, ziffernAnzahl,i); //sinvoller
                //spieler[k].yourTurn(wuerfel, variante, ziffernAnzahl); //erfüllt UML
            }
        }
        beenden();
    }
    /**
     * Auswertung und Ende des Spiels
     */
    public void beenden(){
        System.out.println("Spiel beendet:");
        /**
         * sortieren der Spieler nach Punkten
         */
        for (int i=spieler.length-1; i>=0;i--){
            for (int k=0; k<i; k++){
                if (spieler[k].getSumme()<spieler[k+1].getSumme()){
                    Spieler temp;
                    temp = spieler[k];
                    spieler[k]=spieler[k+1];
                    spieler[k+1]=temp;
                }
            }
        }
        /**
         * Siegerehrung
         */
        for (int i=0;i<=spieler.length-1; i++){
            System.out.println(spieler[i].getName() + " Du bist mit einer Summe von "+String.valueOf(spieler[i].getSumme())+" Punkten " + String.valueOf(i+1) + ". Platz geworden!");
        }
    }
    /**
     * Ausgabe der Spielregeln
     */
    public void printRegeln(){
      System.out.println(regeln);  
    }
}
