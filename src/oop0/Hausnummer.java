package oop0;

/**
 * Speichert und verwaltet eine Hausnummer
 */

public class Hausnummer {
	
	private int[] ziffern;
	/**
         * erzeugt eine Hausnummer mit beliebiger Ziffernlaenge
         * @param ziffernAnz Anzahl der Ziffern fuer eine Hausnummer
         */
	public Hausnummer(int ziffernAnz){
		this.ziffern = new int[ziffernAnz];
		for (int i=0; i<ziffernAnz;i++){
			ziffern[i]=-1;								// -1 symbolisiert unbesetzten hausnummernplatz
		}
	}
	/**
         * erzeugt eine Hausnummer mit Ziffernlaenge 3
         */
	public Hausnummer(){
		this(3);
	}
	/**
         * testet, ob eine Ziffer an die angegebene Position gesetzt werden kann und setzt diese falls moeglich. 
         * @param ziffer zu setzende Ziffer
         * @param platz Position an die die Ziffer gesetzt werden soll. 1-basiert
         * @return true, falls die Ziffer erfolgreich an die gegebene Position gesetzt wurde, false, falls nicht.
         */
	public boolean setZiffer(int ziffer, int platz){	// angenommen platz wird in Umgangssprache (1basiert) angegeben 
		try {
			if (this.ziffern[platz-1]==-1){
				this.ziffern[platz-1]=ziffer;
			return true;								// die Ziffer wurde an den platz gesetzt
			}
			else return false; 							// der Platz der Hausnummer wurde schon besetzt 
		} catch (ArrayIndexOutOfBoundsException e){			
			return false;								// die Hausnummer wird zu lang, (platz nicht vorhanden)
		} catch (Exception e){
                        return false;
                }
	}
	/**
         * gibt die Hausnummer als int-Wert zurueck
         * @return Hausnummer als int
         */
	public int getHausnummer(){
            int result = 0;
            int k=ziffern.length-1;
            for (int i=0; i<=ziffern.length-1;i++){
                result += ziffern[i]*Math.pow(10,k);    //zb. 524 = 5*10^2 +2*10^1 + 4*10^0
                k--;
            }
            return result;
	}

}
