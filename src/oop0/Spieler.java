package oop0;

import java.util.Scanner;
import oop.Wuerfel;

/**
 * modelliert einen Spieler. 
 * @author Greta
 */
public class Spieler {
	private static Scanner scan = new Scanner (System.in);
	
	private Hausnummer[] hausnummern;
	private String name; 
	
/**
 * erzeugt einen neuen Spieler
 * @param name 	 wird als Name des Spielers abgespeichert
 * @param runden gibt die Laenge des Hausnummernarrays an
 */
	public Spieler (String name, int runden){
		this.name=name;
		this.hausnummern= new Hausnummer[runden];					
		//for (int i=0; i<runden;i++){
		//	hausnummern=null;
		//}
	}
        /**
         *	gibt den Namen der Spielers zurueck 	 
         * @return Spielername
         */
	public String getName(){
		return name;
	}

/**
 * gibt die Hausnummer einer bestimmten Runde zurueck
 * @param runde - bestimmt die Runde, dessen Hausnummer zur�ck gegeben wird 
 * @return hausnummer als int-wert
 */
	public int getHausnummer(int runde){
		return hausnummern[runde].getHausnummer(); 				
	}
	
/**
 * Errechnet die Summe aller Hausnummern(Runden) fuer einen Spieler
 * @return Gesamtanzahl aller Runden
 */
	public int getSumme(){
		int r=0; 
		for (Hausnummer nummer: hausnummern){
			r+= nummer.getHausnummer();
		}
		return r; 
	}
        
        /**
         * überladene Funktion yourTurn, diese Funktion entspricht der Modellierung im UML
         * da es jedoch sinnvoller ist die aktuelle Runde mit an die Funktion zu übergeben, existieren 2 Versionen der Funktion yourTurn
         * @param wuerfel uebergibt mit welchem wuerfelobjekt gewuerfelt wird
         * @param variante unterscheidet zwischen Spielvariante 1 und 2 
         * @param ziffAnz gibt an wie lang die Hausnummer wird, also wie oft gewuerfelt wird und wie viele Stellen besetzt werden muessen
         * @return gibt die Hausnummer der gespielten Runde als int-wert zurueck 
         */
        public int yourTurn (Wuerfel wuerfel, int variante, int ziffAnz){       
            for (int i=0; i<=hausnummern.length-1;i++){
                if (hausnummern[i]==null) return yourTurn(wuerfel,variante,ziffAnz,i);
            }
            return -1;
        }
        
/**
 * fuehrt eine Runde fuer jeweils einen Spieler durch. 
 * d.h. es wird (hier) 3x gewuerfelt, die Ziffern werden jeweils im Hausnummernarray abgespeichert, 
 * das Festlegen der Position passiert, entsprechend der gewaehlten variante, vor oder nach dem Wurf
 * @param wuerfel-  uebergibt mit welchem wuerfelobjekt gewuerfelt wird
 * @param variante-	unterscheidet zwischen Spielvariante 1 und 2 
 * @param ziffAnz-  gibt an wie lang die Hausnummer wird, also wie oft gewuerfelt wird und wie viele Stellen besetzt werden muessen
 * @param runde aktuelle runde 0-basiert
 * @return gibt die Hausnummer der gespielten Runde als int-wert zurueck 
 */
	public int yourTurn (Wuerfel wuerfel, int variante, int ziffAnz, int runde){
		Hausnummer hausnummer = new Hausnummer(ziffAnz);
		System.out.println(name + ", du bist dran!");
		int stelle=-1;
		//Variante 1 
		if (variante==1){
                    for (int i =1; i<=ziffAnz; i++){	//******** i=1,2,3 (vgl. aufgaben stellung.) fuer uneingeschraenkte n, muss ziffernanzahl an fkt uebergeben werden. nachtraeglich hinzugefuegt
			//Wuerfelvorgang

                        System.out.println("Dein " + i +". Wurf! Es wird gewuerfelt! Zum Stoppen *Enter* druecken!");
                        if (i>1) scan.nextLine();
                        scan.nextLine();
			int zahl= wuerfel.wuerfeln();
                        System.out.println("Es ist eine " + zahl + "! An welche Stelle der Hausnummer moechtest du diese Ziffer setzen?");
			//Platzierung der Stelle
			Boolean flag = false;
			while(!flag){
                            try{
                                System.out.println("Bitte gib einen Hausnummernplatz an! (1.."+ziffAnz+")");
				stelle= scan.nextInt();
				flag=hausnummer.setZiffer(zahl, stelle);
                            }catch (Exception e){
                                scan = new Scanner (System.in);
                                System.out.println("Prüfe deine Eingabe! Optionen: (1.."+ziffAnz+")");
                            }
			}
                    }
                    hausnummern[runde] = hausnummer;
                    System.out.println("Deine Hausnummer fuer diese Runde ist: "+ hausnummer.getHausnummer());
                    System.out.println("Dein Zug ist beendet!");
                    System.out.println("===================================================================================================================================================");
                    return hausnummer.getHausnummer();
		}
		
		//Variante 2
		else if (variante ==2){
                    for (int i =1; i<=ziffAnz; i++){							//******** i=1,2,3 (vgl. aufgaben stellung.) f�r uneingeschr�nkte n, muss ziffernanzahl an fkt �bergeben werden. nachtraeglich hinzugefuegt
			//wuerfeln
			int zahl= wuerfel.wuerfeln();
			//Platzierung der Stelle
			System.out.println("Dein " + i+ ". Wurf!");
			Boolean flag = false;
			while(!flag){
                            try{
				System.out.println("Bitte gib einen Hausnummernplatz fuer den neachsten Wurf an! (1.."+ziffAnz+")");
				stelle= scan.nextInt();
				flag=hausnummer.setZiffer(zahl, stelle);
                            }catch (Exception e){
                                scan = new Scanner (System.in);
                                System.out.println("Prüfe deine Eingabe! Optionen: (1|2)");
                            }
			}
			//Wuerfelvorgang
			System.out.println("Es wird gewuerfelt! Zum Stoppen *Enter* druecken!");
                        scan.nextLine();
			scan.nextLine();
			System.out.print("Es ist eine " + zahl + "! Sie wurde an den " + stelle + ". Platz gesetzt. ");
                    }
                    hausnummern[runde] = hausnummer;
                    System.out.println("Deine Hausnummer fuer diese Runde ist: "+ hausnummer.getHausnummer());	
                    System.out.println("Dein Zug ist beendet!");
                    System.out.println("===================================================================================================================================================");
                    return hausnummer.getHausnummer();	
		}
		//nicht vorhandene Variante 
		System.out.println("Diese Spielvariante gibt es nicht!");
		return 0;
	}
}


