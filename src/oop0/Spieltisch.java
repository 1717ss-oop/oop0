package oop0;

import java.util.Scanner;

/**
 * Haelt die Main Methode
 * Erstellen und Beenden eines Spiels
 * @author leo
 */

public class Spieltisch {
        private static Scanner scan = new Scanner (System.in);
        
        /**
         * Main Methode Erstellen und Beenden eines Spiels
         * @param args Werden nicht verwendet.
         */
	public static void main(String[] args) {
            
            Spiel spiel = new Spiel();
            while(true){
                spiel.spielen();
                System.out.println("\":quit\" um das Spiel zu beenden. Etwas anderes um fortzufahren.");
                if (scan.next().equals(":quit")) break;
            }
            System.out.println("Bye");
	}

}
